import Foundation
import UIKit

protocol ErrorReceivableDelegate: AnyObject {
    func didReceiveError(_ error: String)
    func handleFailedState(_ state: UseCaseState)
}

extension ErrorReceivableDelegate where Self: UIViewController {
    func didReceiveError(_ error: String) {
        let ac = UIAlertController(title: nil, message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .default)
        ac.addAction(action)
        present(ac, animated: true)
    }
    
    func handleFailedState(_ state: UseCaseState) {
        switch state {
        case .noInternet:
            didReceiveError("No Internet Available")
        default:
            didReceiveError("Oops something went wrong")
        }
    }
}
