import Foundation
import UIKit

class NavigationHelper {
    
    static func homePage() -> FeaturedViewController? {
        guard let home = UIStoryboard.main()?.initView(with: "Posts") as? FeaturedViewController else {
            return nil
        }
        return home
    }
    
    static func contributorPage() -> ContributorViewController? {
        guard let profile = UIStoryboard.main()?.initView(with: "Contributor") as? ContributorViewController else {
            return nil
        }
        return profile
    }
    
    static func postDetailsVC() -> PostDetailsViewController? {
        guard let postDetailsVC = UIStoryboard.main()?.initView(with: "PostDetails") as? PostDetailsViewController else {
            return nil
        }
        return postDetailsVC
    }
    
    static func followDetailsVC(with viewModel: FollowerViewModel) -> FollowerViewController? {
        guard let followerDetailsVC = UIStoryboard.main()?.initView(with: "followers") as? FollowerViewController else {
            return nil
        }
        followerDetailsVC.viewModel = viewModel
        return followerDetailsVC
    }
}

extension UIViewController {
    func loadPostDetails(with post: Post) {
        guard let vc = NavigationHelper.postDetailsVC() else {
            return
        }
        let viewModel = PostDetailsViewModel(with: post)
        vc.viewModel = viewModel
        dismissHUD()
        navigationController?.show(vc, sender: nil)
    }
    
    func loadContributorDetails(with contributor: Contributor) {
        guard let vc = NavigationHelper.contributorPage() else {
            return
        }
        let viewModel = ContributorViewModel(with: contributor)
        vc.viewModel = viewModel
        dismissHUD()
        navigationController?.show(vc, sender: nil)
    }
    
    func loadFollowerDetails(with contributors: [Contributor]) {
        let viewModel = FollowerViewModel(with: contributors)
        guard let vc = NavigationHelper.followDetailsVC(with: viewModel) else {
            return
        }
        dismissHUD()
        navigationController?.show(vc, sender: nil)
    }
}
