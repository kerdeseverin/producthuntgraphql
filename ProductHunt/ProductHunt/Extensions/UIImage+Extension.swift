import Foundation
import Kingfisher
import UIKit

extension UIImageView {
    func loadAsync(from url: String?, placeHolder: UIImage? = nil) {
        guard let url = url else {
            return
        }
        let imageUrl = URL(string: url)
        kf.indicatorType = .activity
        if let placeHolder = placeHolder {
            kf.setImage(with: imageUrl, placeholder: placeHolder)
        } else {
            kf.setImage(with: imageUrl)
        }
    }
}

extension UIImage {
    static func defaultProfileImage() -> UIImage {
        return UIImage.init(systemName: "person.circle.fill")!
    }
}
