import Foundation
import MBProgressHUD

extension UIViewController {
    func showHUD(){
        Thread.runOnMainThread {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func dismissHUD() {
        Thread.runOnMainThread {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}
