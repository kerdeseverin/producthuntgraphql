import Foundation
import UIKit

extension UIStoryboard {
    static func main() -> UIStoryboard? {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    func initView(with identifier: String) -> UIViewController? {
        return instantiateViewController(identifier: identifier)
    }
}
