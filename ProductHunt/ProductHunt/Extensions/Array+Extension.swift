import Foundation

extension Array where Element == Post {
    func lastCursor() -> String {
        return last?.cursor ?? ""
    }
}
