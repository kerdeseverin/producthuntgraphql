import Foundation

extension Thread {
    @objc public class func runOnMainThread(cb: @escaping () -> Void) {
        if Thread.isMainThread {
            cb()
        } else {
            DispatchQueue.main.async {
                cb()
            }
        }
    }
}
