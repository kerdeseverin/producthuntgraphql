import Foundation

//These protocols would have various default implementations based on project needs
public protocol Repository {}
public protocol UseCase {}

public struct UseCaseResult {
    var state: UseCaseState
    var data: AnyObject?
}

//Example project is rather small so we're only using one UseCaseState
//Normally there would be different states for different screens
//Example: a SignUpUseCaseState would have states such as emailError, passwordError,
public enum UseCaseState: Int {
    case loading = 0, success, noInternet, serverError
}
