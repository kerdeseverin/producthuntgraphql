import Foundation
import DifferenceKit
import UIKit

protocol DataContainer {
    associatedtype DataType

    var data: [DataType] { get set }
}
class BaseDataSource<DataType>: NSObject, DataContainer {
    var data: [DataType] = []
    
    func updateData(with newData: [DataType], on tableView: UITableView) {
        data = newData
        tableView.reloadData()
    }
}

extension BaseDataSource where DataType: Differentiable {
    func updateData(with newData: [DataType],
                    on tableView: UITableView,
                    animationStyle: @autoclosure () -> UITableView.RowAnimation = .automatic) {
        let changeSet = StagedChangeset(source: data, target: newData)
        tableView.reload(using: changeSet, with: animationStyle()) { (newData) in
            data = newData
        }
    }
}

extension UITableView {
    func defaultAttributes() {
        backgroundColor = .white
        estimatedRowHeight = 50
        bounces = true
        tableFooterView = UIView()
    }
}
