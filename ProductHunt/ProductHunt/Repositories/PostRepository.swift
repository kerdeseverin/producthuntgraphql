import Foundation

protocol PostRepositoryProtocol: Repository {
    func fetchPosts(cursorPosition: String, updatedState: @escaping (UseCaseResult) -> Void)
}

class PostRepository: PostRepositoryProtocol {
    
    func fetchPosts(cursorPosition: String, updatedState: @escaping (UseCaseResult) -> Void) {
        guard ServerIO.shared.isInternetAvailable() else {
            updatedState(UseCaseResult(state: .noInternet))
            return
        }
        ServerIO.shared.fetchPosts(cursorPosition: cursorPosition) { edges in
            guard let edges = edges else {
                updatedState(UseCaseResult(state: .serverError))
                return
            }
            var allPosts = [Post]()
            edges.forEach({
                let cursor = $0.cursor
                var post = Post(withPostData: $0.node)
                post.cursor = cursor
                allPosts.append(post)
            })
            updatedState(UseCaseResult(state: .success,
                                       data: allPosts as AnyObject))
        }
    }
}
