import Foundation

protocol ContributorRepositoryProtocol: Repository {
    func fetchContributor(by username: String, cursorPosition: String, updatedState: @escaping (UseCaseResult) -> Void)
}

struct ContributorResult {
    var posts: [Post]
    var followers: [Contributor]?
}

class ContributorRepository: ContributorRepositoryProtocol {
    
    
    func fetchContributor(by username: String, cursorPosition: String, updatedState: @escaping (UseCaseResult) -> Void) {
        guard ServerIO.shared.isInternetAvailable() else {
            updatedState(UseCaseResult(state: .noInternet))
            return
        }
        ServerIO.shared.fetchUserDetails(by: username, cursorPosition: cursorPosition) { postEdges, followerEdges in
            guard let postEdges = postEdges else {
                updatedState(UseCaseResult(state: .serverError))
                return
            }
            var allPosts = [Post]()
            var allFollowers = [Contributor]()
            postEdges.forEach({
                let cursor = $0.cursor
                var post = Post(withUserData: $0.node)
                post.cursor = cursor
                allPosts.append(post)
            })
            if let followerEdges = followerEdges {
                followerEdges.forEach({
                    let contribtor = Contributor(with: $0.node.id,
                                                 name: $0.node.name,
                                                 username: $0.node.username,
                                                 imageUrl: $0.node.profileImage)
                    allFollowers.append(contribtor)
                })
            }
            let result = ContributorResult(posts: allPosts, followers: allFollowers)
            updatedState(UseCaseResult(state: .success,
                                       data: result as AnyObject))
        }
    }
}
