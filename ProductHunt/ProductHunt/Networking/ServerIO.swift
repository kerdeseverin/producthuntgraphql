import Foundation
import Apollo
import Reachability

typealias PostNode = GetPostsQuery.Data.Post.Edge.Node
typealias UserPostNode = GetUserQuery.Data.User.VotedPost.Edge.Node
typealias GetPostEdge = GetPostsQuery.Data.Post.Edge
typealias GetUserPostEdge = GetUserQuery.Data.User.VotedPost.Edge
typealias GetUserFollowerEdge = GetUserQuery.Data.User.Follower.Edge

class ServerIO {
    static let shared = ServerIO()
    
    private(set) lazy var apollo: ApolloClient = {
        let cache = InMemoryNormalizedCache()
        let store = ApolloStore(cache: cache)
        let authPayloads = ["Authorization": "Bearer \(Constants.ProductHunt.APIToken)"]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = authPayloads
        let client = URLSessionClient(sessionConfiguration: configuration, callbackQueue: nil)
        let provider = NetworkInterceptorProvider(client: client, shouldInvalidateClientOnDeinit: true, store: store)
        let requestChainTransport = RequestChainNetworkTransport(interceptorProvider: provider,
                                                                 endpointURL: Constants.ProductHunt.baseUrl)
        return ApolloClient(networkTransport: requestChainTransport, store: store)
    }()
    
    func isInternetAvailable() -> Bool {
        let status = Reachability.forInternetConnection()?.currentReachabilityStatus()
        if status == .NotReachable {
            return false
        }
        return true
    }
    
    func fetchPosts(cursorPosition: String, completion: @escaping ([GetPostEdge]?) -> Void) {
        apollo.fetch(query: GetPostsQuery(cursorPosition: cursorPosition)) { result in
            switch result {
            case .success(let nodeData):
                let edges = nodeData.data?.posts.edges
                completion(edges)
                
            case .failure(_):
                completion(nil)
            }
        }
    }
    
    func fetchUserDetails(by username: String, cursorPosition: String, completion: @escaping ([GetUserPostEdge]?, [GetUserFollowerEdge]?) -> Void) {
        apollo.fetch(query: GetUserQuery(username: username, cursorPosition: cursorPosition)) { result in
            switch result {
            case .success(let obj):
                let postEdges = obj.data?.user?.votedPosts.edges
                let followerEdges = obj.data?.user?.followers.edges
                completion(postEdges, followerEdges)
                break
            case .failure(_):
                completion(nil, nil)
            }
        }
    }
}
