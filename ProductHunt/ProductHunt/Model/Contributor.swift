import Foundation
import UIKit

struct Contributor {
    var id: String
    var name: String
    var imageUrl: String?
    var username: String
    var votedOn = [Post]()
    var followers: [Contributor] = []
    
    init(with id: String, name: String, username: String, imageUrl: String?) {
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
        self.username = username
    }
    
    mutating func addPosts(_ posts: [Post]?) {
        let votedOnPosts = posts ?? []
        self.votedOn.append(contentsOf: votedOnPosts)
    }
    
    mutating func addFollowers(_ data: [Contributor]) {
        followers.append(contentsOf: data)
    }
}
