import Foundation

struct Post {
    var id: String
    var cursor: String?
    var name: String
    var url: String
    var tagLine: String
    var votesCount: Int
    var commentsCount: Int
    var thumbnailUrl: String?
    var hunter: Contributor
    var makers = [Contributor]()
    var media = [String]()
    
    init(withPostData node: PostNode) {
        self.id = node.id
        self.name = node.name
        self.url = node.url
        self.tagLine = node.tagline
        self.votesCount = node.votesCount
        self.commentsCount = node.commentsCount
        self.thumbnailUrl = node.thumbnail?.url
        self.hunter = Contributor(with: node.user.id,
                                  name: node.user.name,
                                  username: node.user.username,
                                  imageUrl: node.user.profileImage)
        
        let makers = node.makers.compactMap({  Contributor(with: $0.id,
                                                           name: $0.name,
                                                           username: $0.username,
                                                           imageUrl: $0.profileImage) })
        self.makers.append(contentsOf: makers)
        let urls = node.media.compactMap({ $0.url })
        self.media.append(contentsOf: urls)
    }
    
    //Apollo and Xcode 13 view these as seperate objects. We can clean this up
    init(withUserData node: UserPostNode) {
        self.id = node.id
        self.name = node.name
        self.url = node.url
        self.tagLine = node.tagline
        self.votesCount = node.votesCount
        self.commentsCount = node.commentsCount
        self.thumbnailUrl = node.thumbnail?.url
        self.hunter = Contributor(with: node.user.id,
                                  name: node.user.name,
                                  username: node.user.username,
                                  imageUrl: node.user.profileImage)
        
        let makers = node.makers.compactMap({  Contributor(with: $0.id,
                                                           name: $0.name,
                                                           username: $0.username,
                                                           imageUrl: $0.profileImage) })
        self.makers.append(contentsOf: makers)
        let urls = node.media.compactMap({ $0.url })
        self.media.append(contentsOf: urls)
    }
}
