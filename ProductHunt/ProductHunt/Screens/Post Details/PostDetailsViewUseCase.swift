import Foundation

struct PostDetailsViewUseCase: UseCase {
    var repository: ContributorRepositoryProtocol

    init(with repository: ContributorRepositoryProtocol) {
        self.repository = repository
    }
    
    func fetchContributorDetails(by username: String, updatedState: @escaping (UseCaseResult) -> Void) {
        repository.fetchContributor(by: username, cursorPosition: "", updatedState: updatedState)
    }
}
