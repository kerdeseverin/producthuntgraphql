import UIKit

class PostDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: PostDetailsViewModel!
    private lazy var dataSource = PostDetailsDataSource(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(to: self)
        setupTableView()
        viewModel.fetchData()
    }
    
    private func setupTableView() {
        MediaCell.register(in: tableView)
        AvatarHeaderCell.register(in: tableView)
        ContributorCell.register(in: tableView)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.defaultAttributes()
    }
}

extension PostDetailsViewController: PostDetailsViewModelDelegate {
    
    func didStartFetching() {
        showHUD()
    }
    
    func didFetchContributorDetails(contributor: Contributor) {
        loadContributorDetails(with: contributor)
    }
    
    func didLoadPostData(data: [PostDetailsViewSection]) {
        dismissHUD()
        dataSource.updateData(with: data, on: tableView)
    }
}

extension PostDetailsViewController: PostDetailsDataSourceDelegate {
    func didSelectToFetchMakerFullDetails(maker: Contributor) {
        viewModel.fetchContributorDetails(contributor: maker)
    }
}
