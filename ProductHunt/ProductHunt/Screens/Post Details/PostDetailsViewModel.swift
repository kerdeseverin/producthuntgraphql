import Foundation

protocol PostDetailsViewModelDelegate: ContributorErrorRecievableDelegate {
    func didLoadPostData(data: [PostDetailsViewSection])
}

class PostDetailsViewModel {
    private weak var delegate: PostDetailsViewModelDelegate?
    private var useCase = PostDetailsViewUseCase(with: ContributorRepository())
    private var cursor = ""
    private var post: Post!
    
    // MARK: - Binding
    init(with post: Post) {
        self.post = post
    }
    
    func bind(to delegate: PostDetailsViewModelDelegate) {
        self.delegate = delegate
    }
    
    // MARK: - Main actions
    func fetchData() {
        let data = [PostDetailsViewSection(title: post.name, data: [post as AnyObject], sectionType: .header),
                    PostDetailsViewSection(title: "Makers", data: post.makers as [AnyObject], sectionType: .makers),
                    PostDetailsViewSection(title: "Preview", data: post.media as [AnyObject], sectionType: .media)]
        delegate?.didLoadPostData(data: data)
    }
    
    func fetchContributorDetails(contributor: Contributor) {
        weak var weakSelf = self
        useCase.fetchContributorDetails(by: contributor.username) { result in
            Thread.runOnMainThread {
                switch result.state {
                case .loading:
                    weakSelf?.delegate?.didStartFetching()
                    
                case .success:
                    
                    guard let data = result.data as? ContributorResult,
                          let updatedContributor = weakSelf?.delegate?.processContributorResult(data: data, contributor: contributor) else {
                              weakSelf?.delegate?.handleFailedState(.serverError)
                              return
                          }
                    weakSelf?.delegate?.didFetchContributorDetails(contributor: updatedContributor)
                    
                default:
                    weakSelf?.delegate?.handleFailedState(result.state)
                }
            }
        }
    }
}
