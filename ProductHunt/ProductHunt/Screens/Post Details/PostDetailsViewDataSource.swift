import Foundation
import UIKit

protocol PostDetailsDataSourceDelegate: AnyObject {
    func didSelectToFetchMakerFullDetails(maker: Contributor)
}

enum PostDetailsViewTableSections: Int {
    case header = 0, makers, media
}

struct PostDetailsViewSection {
    var title: String
    var data: [AnyObject]
    var sectionType: PostDetailsViewTableSections
}

class PostDetailsDataSource: BaseDataSource<PostDetailsViewSection>, UITableViewDataSource, UITableViewDelegate {
    private weak var delegate: PostDetailsDataSourceDelegate?
    
    init(delegate: PostDetailsDataSourceDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].data.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentSection = data[indexPath.section].sectionType
        switch currentSection {
        case .header:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AvatarHeaderCell.getDefaultReuseIdentifier(), for: indexPath) as? AvatarHeaderCell,
                  let post = data[indexPath.section].data[indexPath.row] as? Post else {
                      return UITableViewCell()
                  }
            cell.fill(with: post)
            return cell
            
        case .makers:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ContributorCell.getDefaultReuseIdentifier(), for: indexPath) as? ContributorCell,
                  let maker = data[indexPath.section].data[indexPath.row] as? Contributor else {
                      return UITableViewCell()
                  }
            cell.fill(with: maker)
            return cell
            
        case .media:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MediaCell.getDefaultReuseIdentifier(), for: indexPath) as? MediaCell,
                  let mediaUrl = data[indexPath.section].data[indexPath.row] as? String else {
                      return UITableViewCell()
                  }
            cell.fill(with: mediaUrl)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentSection = data[indexPath.section].sectionType
        guard let maker = data[indexPath.section].data[indexPath.row] as? Contributor, currentSection == .makers else {
            return
        }
        delegate?.didSelectToFetchMakerFullDetails(maker: maker)
    }
}
