import Foundation

protocol ContributorViewModelDelegate: ErrorReceivableDelegate {
    func didLoadData(data: [ContributorViewSection])
    func didStartFetching()
}

class ContributorViewModel {
    private weak var delegate: ContributorViewModelDelegate?
    private var useCase = ContributorUseCase(with: ContributorRepository())
    private var cursor = ""
    private var contributor: Contributor!
    
    // MARK: - Binding
    init(with contributor: Contributor) {
        self.contributor = contributor
    }
    
    func bind(to delegate: ContributorViewModelDelegate) {
        self.delegate = delegate
    }
    
    // MARK: - Main actions
    func refreshData() {
        let data = [ContributorViewSection(title: contributor.name, data: [contributor as AnyObject], sectionType: .summary),
                    ContributorViewSection(title: "Voted On", data: contributor.votedOn as [AnyObject], sectionType: .votedOn)]
        delegate?.didLoadData(data: data)
    }
    
    func fetchData() {
        weak var weakSelf = self
        useCase.fetchContributorDetails(by: contributor.username, cursorPosition: cursor) { result in
            Thread.runOnMainThread {
                switch result.state {
                case .loading:
                    weakSelf?.delegate?.didStartFetching()
                    
                case .success:
                    
                    let data = result.data as? ContributorResult
                    let posts = data?.posts ?? []
                    let followers = data?.followers ?? []
                    weakSelf?.cursor = posts.lastCursor()
                    weakSelf?.contributor.addPosts(posts)
                    weakSelf?.contributor.addFollowers(followers)
                    weakSelf?.refreshData()
                    
                default:
                    weakSelf?.delegate?.handleFailedState(result.state)
                }
            }
        }
    }
    
    func getContributorFollowers() -> [Contributor] {
        return contributor.followers
    }
}
