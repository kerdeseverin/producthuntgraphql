import UIKit
import Foundation

protocol ContributorDataSourceDelegate: AnyObject {
    func didSelectToLoadPostDetails(post: Post)
    func didSelectToViewFollowers()
    func fetchMoreData()
}

enum ContributorViewTableSections: Int {
    case summary = 0, votedOn
}

struct ContributorViewSection {
    var title: String
    var data: [AnyObject]
    var sectionType: ContributorViewTableSections
}

class ContributorDataSource: BaseDataSource<ContributorViewSection>, UITableViewDataSource, UITableViewDelegate {
    private weak var delegate: ContributorDataSourceDelegate?
    
    init(delegate: ContributorDataSourceDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].data.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].title
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentSection = data[indexPath.section].sectionType
        switch currentSection {
        case .summary:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AvatarHeaderCell.getDefaultReuseIdentifier(), for: indexPath) as? AvatarHeaderCell,
                    let contributor = data[indexPath.section].data[indexPath.row] as? Contributor else {
                return UITableViewCell()
            }
            cell.fill(with: contributor)
            return cell
            
        case .votedOn:

            guard let cell = tableView.dequeueReusableCell(withIdentifier: AvatarHeaderCell.getDefaultReuseIdentifier(), for: indexPath) as? AvatarHeaderCell,
                    let post = data[indexPath.section].data[indexPath.row] as? Post else {
                return UITableViewCell()
            }
            cell.fill(with: post)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentSection = data[indexPath.section].sectionType
        guard let post = data[indexPath.section].data[indexPath.row] as? Post, currentSection == .votedOn else {
            delegate?.didSelectToViewFollowers()
            return
        }
        delegate?.didSelectToLoadPostDetails(post: post)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentSection = data[indexPath.section].sectionType
        guard currentSection == .votedOn, !data.isEmpty else {
            return
        }
        if indexPath.row == data[indexPath.section].data.count-1 {
            delegate?.fetchMoreData()
        }
    }
}
