import UIKit

class ContributorViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: ContributorViewModel!
    
    private lazy var dataSource = ContributorDataSource(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(to: self)
        viewModel.refreshData()
        setupTableView()
    }
    
    private func setupTableView() {
        AvatarHeaderCell.register(in: tableView)
        tableView.defaultAttributes()
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    }
}

extension ContributorViewController: ContributorViewModelDelegate {
    func didStartFetching() {
        showHUD()
    }
    
    func didLoadData(data: [ContributorViewSection]) {
        dismissHUD()
        dataSource.updateData(with: data, on: tableView)
    }
}

extension ContributorViewController: ContributorDataSourceDelegate {
    
    func didSelectToLoadPostDetails(post: Post) {
        loadPostDetails(with: post)
    }
    
    func didSelectToViewFollowers() {
        loadFollowerDetails(with: viewModel.getContributorFollowers())
    }
    
    func fetchMoreData() {
        viewModel.fetchData()
    }
}
