import Foundation

struct ContributorUseCase: UseCase {
    var repository: ContributorRepositoryProtocol

    init(with repository: ContributorRepositoryProtocol) {
        self.repository = repository
    }
    
    func fetchContributorDetails(by username: String, cursorPosition: String, updatedState: @escaping (UseCaseResult) -> Void) {
        repository.fetchContributor(by: username, cursorPosition: cursorPosition, updatedState: updatedState)
    }
}
