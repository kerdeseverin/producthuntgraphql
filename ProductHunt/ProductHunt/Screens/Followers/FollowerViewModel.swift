//
//  FollowerViewModel.swift
//  ProductHunt
//
//  Created by kerde on 2021-11-09.
//

import Foundation

protocol ContributorErrorRecievableDelegate: ErrorReceivableDelegate {
    func didStartFetching()
    func didLoadData(data: [Contributor])
    func didFetchContributorDetails(contributor: Contributor)
    func processContributorResult(data: ContributorResult?, contributor: Contributor) -> Contributor
}

extension ContributorErrorRecievableDelegate {
    func didLoadData(data: [Contributor]) {}
    func processContributorResult(data: ContributorResult?, contributor: Contributor) -> Contributor {
        var updatedContributor = contributor
        let posts = data?.posts
        let followers = data?.followers ?? []
        updatedContributor.addPosts(posts)
        updatedContributor.addFollowers(followers)
        return updatedContributor
    }
}

protocol FollowerViewModelDelegate: ContributorErrorRecievableDelegate {}

class FollowerViewModel {
    
    private var followers: [Contributor]!
    private var useCase = PostDetailsViewUseCase(with: ContributorRepository())
    weak var delegate: FollowerViewModelDelegate?
    
    init(with contributors:[Contributor]) {
        self.followers = contributors
    }
    
    func bind(to delegate: FollowerViewModelDelegate) {
        self.delegate = delegate
    }
    
    func refreshData() {
        delegate?.didLoadData(data: followers)
    }
    
    func fetchContributorDetails(contributor: Contributor) {
        weak var weakSelf = self
        useCase.fetchContributorDetails(by: contributor.username) { result in
            Thread.runOnMainThread {
                switch result.state {
                case .loading:
                    weakSelf?.delegate?.didStartFetching()
                    
                case .success:
                    
                    guard let data = result.data as? ContributorResult,
                          let updatedContributor = weakSelf?.delegate?.processContributorResult(data: data, contributor: contributor) else {
                              weakSelf?.delegate?.handleFailedState(.serverError)
                              return
                          }
                    weakSelf?.delegate?.didFetchContributorDetails(contributor: updatedContributor)
                    
                default:
                    weakSelf?.delegate?.handleFailedState(result.state)
                }
            }
        }
    }
}
