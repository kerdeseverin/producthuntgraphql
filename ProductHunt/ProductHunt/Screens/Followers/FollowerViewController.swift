//
//  FollowerViewController.swift
//  ProductHunt
//
//  Created by kerde on 2021-11-09.
//

import Foundation
import UIKit

class FollowerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: FollowerViewModel!
    
    private lazy var dataSource = FollowerDataSource(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(to: self)
        viewModel.refreshData()
        setupTableView()
    }
    
    private func setupTableView() {
        ContributorCell.register(in: tableView)
        tableView.defaultAttributes()
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    }
}

extension FollowerViewController: FollowerViewModelDelegate {
    func didStartFetching() {
        showHUD()
    }
    
    func didFetchContributorDetails(contributor: Contributor) {
        loadContributorDetails(with: contributor)
    }
    
    func didLoadData(data: [Contributor]) {
        dataSource.updateData(with: data, on: tableView)
    }
}

extension FollowerViewController: FollowerDataSourceDelegate {
    
    func didSelectToSeeMoreDetails(for contributor: Contributor) {
        viewModel.fetchContributorDetails(contributor: contributor)
    }
    
}
