import UIKit
import Foundation

protocol FollowerDataSourceDelegate: AnyObject {
    func didSelectToSeeMoreDetails(for user: Contributor)
}

class FollowerDataSource: BaseDataSource<Contributor>, UITableViewDataSource, UITableViewDelegate {
    private weak var delegate: FollowerDataSourceDelegate?
    
    init(delegate: FollowerDataSourceDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contributor = data[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ContributorCell.getDefaultReuseIdentifier(), for: indexPath) as? ContributorCell else {
            return UITableViewCell()
        }
        cell.fill(with: contributor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let contributor = data[indexPath.row]
        delegate?.didSelectToSeeMoreDetails(for: contributor)
    }
}
