import Foundation
import UIKit

@objcMembers class AvatarHeaderCell: UITableViewCell {
    // MARK: - Overrides
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Class Functions
    
    public class func register(in tableView: UITableView, for reuseIdentifier: String? = nil) {
        let bundle = Bundle(for: AvatarHeaderCell.self)
        let nib = UINib(nibName: AvatarHeaderCell.getDefaultReuseIdentifier(), bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier
                           ?? AvatarHeaderCell.getDefaultReuseIdentifier())
    }
    
    public class func getDefaultReuseIdentifier() -> String {
        return "AvatarHeaderCell"
    }
    
    // MARK: - Render
    
    func fill(with post: Post) {
        avatarImage.loadAsync(from: post.thumbnailUrl)
        titleLabel.text = post.name
        descriptionLabel.text = "\(post.tagLine)\n\nHunted by: \(post.hunter.name)"
    }
    
    func fill(with contributor: Contributor) {
        avatarImage.loadAsync(from: contributor.imageUrl, placeHolder: UIImage.defaultProfileImage())
        titleLabel.text = contributor.name
        descriptionLabel.text = "\(contributor.username)"
    }
}
