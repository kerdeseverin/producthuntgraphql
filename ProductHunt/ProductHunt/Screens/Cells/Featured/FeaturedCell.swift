import Foundation
import UIKit

@objcMembers class FeaturedCell: UITableViewCell {
    // MARK: - Overrides
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var tagLine: UILabel!
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var hunterButton: UIButton!
    
    var onHunterTapped: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        setupWidget()
    }

    // MARK: - Class Functions

    public class func register(in tableView: UITableView, for reuseIdentifier: String? = nil) {
        let bundle = Bundle(for: FeaturedCell.self)
        let nib = UINib(nibName: FeaturedCell.getDefaultReuseIdentifier(), bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier
            ?? FeaturedCell.getDefaultReuseIdentifier())
    }

    public class func getDefaultReuseIdentifier() -> String {
        return "FeaturedCell"
    }

    // MARK: - Private Functions

    func setupWidget() {
        //Ideally more customizations would be here
        hunterButton.setTitleColor(.blue, for: .normal)
        hunterButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
    }

    // MARK: - Render

    func fill(with post: Post) {
        profileImage.loadAsync(from: post.thumbnailUrl)
        productTitle.text = post.name
        tagLine.text = post.tagLine
        votes.text = "Votes: \(post.votesCount)"
        comments.text = " Comments: \(post.commentsCount)"
        hunterButton.setTitle("Hunter: \(post.hunter.name)", for: .normal)
    }
    
    @IBAction func hunterButtonTapped(_ sender: Any) {
        onHunterTapped?()
    }
}
