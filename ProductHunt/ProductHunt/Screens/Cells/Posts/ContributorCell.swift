import Foundation
import UIKit

@objcMembers class ContributorCell: UITableViewCell {
    // MARK: - Overrides
    @IBOutlet weak var makerTitle: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    
    // MARK: - Class Functions

    public class func register(in tableView: UITableView, for reuseIdentifier: String? = nil) {
        let bundle = Bundle(for: ContributorCell.self)
        let nib = UINib(nibName: ContributorCell.getDefaultReuseIdentifier(), bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier
            ?? ContributorCell.getDefaultReuseIdentifier())
    }

    public class func getDefaultReuseIdentifier() -> String {
        return "ContributorCell"
    }

    // MARK: - Render

    func fill(with maker: Contributor) {
        makerTitle.text = maker.name
        coverImage.loadAsync(from: maker.imageUrl, placeHolder: UIImage.defaultProfileImage())
    }
}
