import Foundation
import UIKit

@objcMembers class MediaCell: UITableViewCell {
    // MARK: - Overrides
    @IBOutlet weak var mediaImage: UIImageView!

    // MARK: - Class Functions

    public class func register(in tableView: UITableView, for reuseIdentifier: String? = nil) {
        let bundle = Bundle(for: MediaCell.self)
        let nib = UINib(nibName: MediaCell.getDefaultReuseIdentifier(), bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier
            ?? MediaCell.getDefaultReuseIdentifier())
    }

    public class func getDefaultReuseIdentifier() -> String {
        return "MediaCell"
    }

    // MARK: - Render

    func fill(with mediaUrl: String) {
        mediaImage.loadAsync(from: mediaUrl)
    }
}
