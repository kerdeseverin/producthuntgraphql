import Foundation
import UIKit

protocol FeaturedDataSourceDelegate: AnyObject {
    func didTapToViewMoreDetails(for user: Contributor)
    func didTapToViewMoreDetails(for post: Post)
    func fetchMoreData()
}

class FeaturedDataSource: BaseDataSource<Post>, UITableViewDataSource, UITableViewDelegate {
    private weak var delegate: FeaturedDataSourceDelegate?
    
    init(delegate: FeaturedDataSourceDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = data[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeaturedCell.getDefaultReuseIdentifier(), for: indexPath) as? FeaturedCell else {
            return UITableViewCell()
        }
        cell.fill(with: post)
        cell.onHunterTapped = { [weak self] in
            self?.delegate?.didTapToViewMoreDetails(for: post.hunter)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let post = data[indexPath.row]
        delegate?.didTapToViewMoreDetails(for: post)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !data.isEmpty, indexPath.row == data.count-1 {
            delegate?.fetchMoreData()
        }
    }
}
