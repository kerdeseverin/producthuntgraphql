import Foundation

struct FeaturedViewUseCase: UseCase {
    var postRepository: PostRepositoryProtocol
    var contributorRepository: ContributorRepositoryProtocol

    init(with postRepository: PostRepositoryProtocol, contributorRepository: ContributorRepositoryProtocol) {
        self.postRepository = postRepository
        self.contributorRepository = contributorRepository
    }
    
    func fetchPostData(position: String, updatedState: @escaping (UseCaseResult) -> Void) {
        updatedState(UseCaseResult(state: .loading))
        postRepository.fetchPosts(cursorPosition: position, updatedState: updatedState)
    }
    
    func fetchContributorDetails(by username: String, updatedState: @escaping (UseCaseResult) -> Void) {
        updatedState(UseCaseResult(state: .loading))
        contributorRepository.fetchContributor(by: username, cursorPosition: "", updatedState: updatedState)
    }
}
