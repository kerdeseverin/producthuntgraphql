import Foundation

protocol FeaturedViewModelDelegate: ErrorReceivableDelegate {
    func didStartFetching()
    func didRetrieveDataFromFetch(data: [Post])
    func didFetchContributorDetails(contributor: Contributor)
}

class FeaturedViewModel {
    private weak var delegate: FeaturedViewModelDelegate?
    private var useCase = FeaturedViewUseCase(with: PostRepository(),
                                              contributorRepository: ContributorRepository())
    private var cursor = ""
    private var postData = [Post]()
    
    // MARK: - Binding
    
    func bind(to delegate: FeaturedViewModelDelegate) {
        self.delegate = delegate
    }
    
    // MARK: - Main actions
    func fetchData() {
        weak var weakSelf = self
        useCase.fetchPostData(position: cursor) { result in
            Thread.runOnMainThread {
                switch result.state {
                case .loading:
                    weakSelf?.delegate?.didStartFetching()
                    
                case .success:
                    let data = result.data as? [Post] ?? []
                    weakSelf?.cursor = data.lastCursor()
                    weakSelf?.postData.append(contentsOf: data)
                    weakSelf?.delegate?.didRetrieveDataFromFetch(data: weakSelf?.postData ?? [])
                    
                default:
                    weakSelf?.delegate?.handleFailedState(result.state)
                }
            }
        }
    }
    
    //fetchContributorDetails is part of other viewMdoels.
    //We can follow DRY by using protocols or subclassing. Leaving for now as its a good discussion point related to architecture
    func fetchContributorDetails(contributor: Contributor) {
        weak var weakSelf = self
        useCase.fetchContributorDetails(by: contributor.username) { result in
            Thread.runOnMainThread {
                switch result.state {
                case .loading:
                    weakSelf?.delegate?.didStartFetching()
                    
                case .success:
                    
                    var updatedContributor = contributor
                    updatedContributor.addPosts(result.data as? [Post])
                    weakSelf?.delegate?.didFetchContributorDetails(contributor: updatedContributor)
                    
                default:
                    weakSelf?.delegate?.handleFailedState(result.state)
                }
            }
        }
    }
}
