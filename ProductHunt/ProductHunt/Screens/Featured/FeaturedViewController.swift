import UIKit

class FeaturedViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = FeaturedViewModel()
    private lazy var dataSource = FeaturedDataSource(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(to: self)
        setupTableView()
        viewModel.fetchData()
    }
    
    private func setupTableView() {
        FeaturedCell.register(in: tableView)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.defaultAttributes()
    }
}

extension FeaturedViewController: FeaturedViewModelDelegate {
    
    func didStartFetching() {
        showHUD()
    }
    
    func didRetrieveDataFromFetch(data: [Post]) {
        dismissHUD()
        dataSource.updateData(with: data, on: tableView)
    }
}

extension FeaturedViewController: FeaturedDataSourceDelegate {
    func fetchMoreData() {
        viewModel.fetchData()
    }
    
    func didTapToViewMoreDetails(for post: Post) {
        loadPostDetails(with: post)
    }
    
    func didTapToViewMoreDetails(for user: Contributor) {
        viewModel.fetchContributorDetails(contributor: user)
    }
    
    func didFetchContributorDetails(contributor: Contributor) {
        loadContributorDetails(with: contributor)
    }
}
