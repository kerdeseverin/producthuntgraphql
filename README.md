# Product Hunt Example Project

## Setting Up The Project
### Cocoapods
Cocoapods is a 3rd party package manager. CocoaPods is built with Ruby and it will be installable with the default Ruby available on macOS. Using the default Ruby install will require you to use sudo when installing gems.
More information can be found here : https://guides.cocoapods.org/using/getting-started.html

    $ sudo gem install cocoapods

##### Installing / Updating Pod Dependencies
In the root of the project directory, run :

    $ pod install

or 

    $ pod update

The following pods are used to speed up development time:
* DifferenceKit
* Reachability
* Apollo
* Kingfisher
* MBProgressHUD

---

Apollo is used to work with graphQL and Swift. 
Queries are found in the Queries.graphql file.
In order for the project to compile, ensure you have apollo cli installed on your machine using brew.
This is needed as there is a run script that executes when project is built.
See: https://www.apollographql.com/docs/ios/tutorial/tutorial-introduction/

```
if [ $ACTION = "indexbuild" ]; then exit 0; fi

SCRIPT_PATH="${PODS_ROOT}/Apollo/scripts"
cd "${SRCROOT}/${TARGET_NAME}"
"${SCRIPT_PATH}"/run-bundled-codegen.sh codegen:generate --target=swift --includes=./**/*.graphql --localSchemaFile="schema.json" API.swift

```
---

### Project Structure and Considerations
#### StoryBoards and Cells:

From our first discussion on Storyboards being difficult to manage, I decided to use them here to show how they can be used albiet in a barebones manner. Ideally I would opt for not using them as this allows us to initialize our ViewControllers with a convenience initializer that takes in a viewModel of a certain type. 

Cells are also done using xibs but this can easily be done programmatically or we can completely use SwiftUI instead.

---

#### Design Pattern:
I've created a diagram in Figma on how the code is structured use MVVM and UseCases. **This can be found in the Images folder of this project.** 
This project is rather small/basic so the true benefits of this pattern are not fully realized. I've left some minor comments in code as well to be discussed.
